package com.herokuapp.matthewmoore;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	double fahrenheit;
	double celsius;
	Scanner input;

	input = new Scanner(System.in);
	System.out.println("Enter the temperate in degrees Fahrenheit: ");
	fahrenheit = input.nextDouble();
	input.close();

	celsius = (fahrenheit - 32) * 5/9;
	System.out.println("The temperature in degrees Celsius is: " + Math.round(celsius));
    }
}
